<!DOCTYPE html>
<html>
<body>    
    <?php if (!empty($results)) : ?>
        <select class="form-control">
            <?php foreach ($results as $program) : ?>
                <option value="<?php echo $program['id']; ?>">
                    <?php echo $program['name']; ?>
                </option>
            <?php endforeach; ?>
        </select>
    <?php else : ?>
        <p>No results found.</p>
    <?php endif; ?>
</body>
</html>
