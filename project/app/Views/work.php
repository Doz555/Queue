<!DOCTYPE html>
<html>

<body>
    <?php if (!empty($work)) : ?>
        <div class="container">
            <div id="work_id" class="d-none"><?php echo $work['id']; ?></div>
            <div class="row">
                <div class="col">
                    <h5>ชื่อคิวงาน:</h5>
                </div>
                <div class="col">
                    <p> <?php echo $work['name']; ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h5>วัตถุประสงค์:</h5>
                </div>
                <div class="col">
                    <p><?php echo $work['purpose']; ?> </p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h5>ชื่อโปรแกรม:</h5>
                </div>
                <div class="col">
                    <p><?php echo $work['program']; ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h5>แผนก:</h5>
                </div>
                <div class="col">
                    <p><?php echo $work['department']; ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h5 >วันที่เริ่ม:</h5>
                </div>
                <div class="col">
                    <p id="work_start"><?php echo $work['start']; ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h5>วันสิ้นสุด:</h5>
                </div>
                <div class="col">
                    <p id="work_end"><?php echo $work['end']; ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h5>ชม.ทำงาน:</h5>
                </div>
                <div class="col">
                    <p id="work_hour"><?php echo $work['hour']; ?> ชั่วโมง</p>
                </div>
            </div>
        </div>
    <?php else : ?>
        <p>No results found.</p>
    <?php endif; ?>
</body>

</html>