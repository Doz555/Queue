<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/home.css">
  <title>ระบบคิวงาน</title>
</head>

<body style="background: beige;">
  <button id="But_Modal1" type="button" class="btn btn-primary d-none" data-toggle="modal" data-target="#myModal1">Open Modal</button>
  <div class="modal" id="myModal1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">ข้อมูล</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer">
          <button id="decreaseTimeBtn" type="button" class="btn btn-primary btn-xs">เพิ่มเวลา</button>
          <button id="increaseTimeBtn" type="button" class="btn btn-primary btn-xs">ลดเวลา</button>
        </div>
      </div>
    </div>
  </div>
  <div class="container mt-5">
    <div style="display: flex;justify-content: space-between;align-items: center;">
      <h2>ระบบคิวงาน</h2>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">เพิ่มคิวงาน</button>
    </div>
    <table class="table" style="background: white;" id="job-table">
      <thead>
        <tr>
          <th>Date</th>
          <th>08:00</th>
          <th>09:00</th>
          <th>10:00</th>
          <th>11:00</th>
          <th>12:00</th>
          <th>13:00</th>
          <th>14:00</th>
          <th>15:00</th>
          <th>16:00</th>
        </tr>
      </thead>
      <tbody id="job-table-body">
      </tbody>
    </table>
  </div>
  <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">เพิ่มคิวงาน</h5>
          <button type="button" class="close" data-dismiss="modal" id="But_Modal2" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="taskName">ชื่อคิวงาน</label>
              <input type="text" class="form-control" id="taskName" required>
            </div>
            <div class="form-group col-md-6">
              <label for="taskColor">สี</label>
              <div class="color-picker">
                <div class="color-slider">
                  <input type="range" min="0" max="360" value="0" step="1" id="hue-slider">
                </div>
                <div id="color-preview" class="preColor"></div>
              </div>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="purpose">วัตถุประสงค์</label>
              <textarea class="form-control" id="purpose" style="resize: auto;" required></textarea>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="programName">ชื่อโปรแกรม</label>
              <?php if (!empty($search_results)) : ?>
                <select class="form-control" id="program">
                  <?php foreach ($search_results as $program) : ?>
                    <option value="<?php echo $program['id']; ?>">
                      <?php echo $program['name']; ?>
                    </option>
                  <?php endforeach; ?>
                </select>
              <?php else : ?>
                <p>No results found.</p>
              <?php endif; ?>
            </div>
            <div class="form-group col-md-6">
              <label for="department">แผนก</label>
              <?php if (!empty($results)) : ?>
                <select class="form-control" id="department">
                  <?php foreach ($results as $program) : ?>
                    <option value="<?php echo $program['id']; ?>">
                      <?php echo $program['name']; ?>
                    </option>
                  <?php endforeach; ?>
                </select>
              <?php else : ?>
                <p>No results found.</p>
              <?php endif; ?>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="start-date-picker">วันที่เริ่ม:</label>
              <input type="date" id="start-date-picker" class="form-control">
            </div>
            <div class="form-group col-md-6">
              <label for="start-time-picker">เวลา:</label>
              <input type="number" id="start-time-picker" min="8" max="16" value="8" required class="form-control">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="end-date-picker">วันสิ้นสุด:</label>
              <input type="date" id="end-date-picker" class="form-control">
            </div>
            <div class="form-group col-md-6">
              <label for="end-time-picker">เวลา:</label>
              <input type="number" id="end-time-picker" min="9" max="17" value="9" required class="form-control">
            </div>
          </div>
          <div style="display: flex; justify-content: flex-end;">
            <button onclick="generateData()" class="btn btn-primary">เพิ่มคิว</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="/js/home.js"></script>
  <script>
    $.each(<?php echo json_encode($work); ?>, function(key, value) {
      work[value["id"]] = value;
    });
    $.each(<?php echo json_encode($queue); ?>, function(key, value) {
      simple[value["date"]] = {
        date: value["date"],
        time: JSON.parse(value['time'])
      };
    });

   
  </script>

</body>

</html>