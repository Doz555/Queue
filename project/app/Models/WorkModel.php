<?php

namespace App\Models;

use CodeIgniter\Model;

class WorkModel extends Model
{
    protected $table = 'work';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'name', 'color', 'purpose', 'program', 'department', 'start', 'end', 'hour', 'change_time'
    ];

    public function updateTime($data)
    {
        $dataArray = json_decode($data, true); // Convert JSON string to associative array

        if (!is_array($dataArray)) {
            return false; // Invalid data format
        }

        foreach ($dataArray as $key => $record) {
           
            if ($key != $dataArray['new_id'] && $record['id'] != '0' && $key != 'old_id' && $key != 'new_id') { 
               // return $key.$dataArray['new_id'];
                $existingRecord = $this->where('id', $record['id'])->first();
                if ($existingRecord) {
                    $data = [
                        'start' => $record['start'],
                        'end' => $record['end']
                    ];
                    $this->update($existingRecord['id'], $data);
                } 
            }
        }

        return true;
    }
    public function EditTime($data)
    {
        $dataArray = json_decode($data, true);
        $currentChangeTime = $this->select('change_time')->where('id', $dataArray['id'])->first();
        //return $currentChangeTime['change_time'] + $dataArray["change_time"].'|'. $dataArray["id"].'|'. $dataArray["end"];
        $updatedData = [
            'hour' => $dataArray["hour"],
            'change_time' => $currentChangeTime['change_time'] + $dataArray["change_time"],
        ];

        $query = $this->where('id', $dataArray['id'])->set($updatedData)->update();

        return $query;
    }


    public function getWorkData($workId)
    {
        $this->select('work.id, work.name, work.color, work.purpose, program.name AS program, department.name AS department, work.start, work.end, work.hour, work.change_time');
        $this->join('program', 'program.id = work.program');
        $this->join('department', 'work.department = department.id');
        $this->where('work.id', $workId);
        $query = $this->first();

        return $query;
    }


    public function insertWork($data)
    {
        $this->insert($data); // Insert the data into the 'work' table
        return $this->insertID(); // Return the inserted record's ID
    }
    public function update_hour($id, $hour)
    {
        $this->set('hour', $hour);
        $this->where('id', $id);
        return $this->update();
    }
}
