<?php

namespace App\Models;

use CodeIgniter\Model;

class QueueModel extends Model
{
    protected $table = 'queue'; // Set the table name
    protected $primaryKey = 'id'; // Set the primary key column(s)
    protected $allowedFields = ['date','time','is_active']; // Define the allowed fields

    public function updateOrInsertQueue($data)
    {
        $dataArray = json_decode($data, true); // Convert JSON string to associative array

        if (!is_array($dataArray)) {
            return false; // Invalid data format
        }

        foreach ($dataArray as $key => $record) {
       
            $date = $record['date'];
            $time = json_encode($record['time']); // Convert time array to JSON string
           
            $existingRecord = $this->where('date', $date)->first(); 
            
            if ($existingRecord) {
                if ($existingRecord['time'] != $time) {
                    $data = [
                        'time' => $time
                    ];
                    $this->where('date', $date)->update($existingRecord['id'], $data);
                }
            } else {
                $newRecord = [
                    'date' => $date,
                    'time' => $time
                ];
                $this->insert($newRecord);
            }
        }

        return true;
    }
}
