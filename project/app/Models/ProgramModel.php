<?php
namespace App\Models;
use CodeIgniter\Model;


class ProgramModel extends Model {
  
    protected $table = 'program';
    
    public function searchPrograms($keyword) {
      return $this->like('name', $keyword)->findAll();
    }
    
  }