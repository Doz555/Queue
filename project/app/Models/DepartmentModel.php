<?php
namespace App\Models;
use CodeIgniter\Model;


class DepartmentModel extends Model {
  
    protected $table = 'department';
    
    public function searchPrograms($keyword) {
      return $this->like('name', $keyword)->findAll();
    }
    
  }