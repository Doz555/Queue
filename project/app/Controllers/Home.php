<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\WorkModel;
use App\Models\ProgramModel;
use App\Models\DepartmentModel;
use App\Models\QueueModel;

class Home extends BaseController
{
    public function index()
    {
        $programModel = new ProgramModel();
        $departmentModel = new DepartmentModel();
        $workModel = new WorkModel();
        $queueModel = new QueueModel();
        $data['results'] = $programModel->findAll();
        $data['search_results'] = $departmentModel->findAll();
        $data['work'] = $workModel->select('id,name,color')->where("DATE(STR_TO_DATE(end, '%d/%m/%Y %H:%i:%s')) > DATE_SUB(DATE_ADD(CURRENT_DATE(),INTERVAL 543 YEAR),INTERVAL 1 DAY)")->get()->getResult();
        $data['queue'] = $queueModel->select('*')->where("date > DATE_SUB(DATE_ADD(CURRENT_DATE(),INTERVAL 543 YEAR),INTERVAL 1 DAY)")->get()->getResult();
        return view('index', $data);
    }
    public function updateTime()
    {
        $model = new WorkModel();
        $jsonData = file_get_contents('php://input');
        
        $result = $model->updateTime($jsonData);
        
        if ($result) {
            $response['status'] = 'success';
            $response['message'] = 'addQueue successfully.';
        } else {
            $response['status'] = 'error';
            $response['message'] = 'addQueue failed.';
        }
        return $this->response->setJSON($response);
    }
    public function EditTime()
    {
        $model = new WorkModel();
        $jsonData = file_get_contents('php://input');
        $result = $model->EditTime($jsonData);
       /* $response['status'] = 'success';
        $response['message'] = $result;
        return $this->response->setJSON($response);*/
        if ($result) {
            $response['status'] = 'success';
            $response['message'] = 'addQueue successfully.';
        } else {
            $response['status'] = 'error';
            $response['message'] = 'addQueue failed.';
        }
        return $this->response->setJSON($response);
    }

    public function get_data()
    {
        $workModel = new WorkModel();
        $workId = $this->request->getGet('id');
        $data['work'] = $workModel->getWorkData($workId);
        return view('work', $data);
    }
    public function addQueue()
    {
        $model = new QueueModel();
        $jsonData = file_get_contents('php://input');
        $result = $model->updateOrInsertQueue($jsonData);
        if ($result) {
            $response['status'] = 'success';
            $response['message'] = 'addQueue successfully.';
        } else {
            $response['status'] = 'error';
            $response['message'] = 'addQueue failed.';
        }
        return $this->response->setJSON($response);
    }
    public function update_hour()
    {
        $model = new WorkModel();
        $id = $this->request->getVar('id');
        $hour = $this->request->getVar('hour');
        $result = $model->update_hour($id, $hour);

        if ($result) {
            $response['status'] = 'success';
            $response['message'] = 'Change time updated successfully.';
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Update failed.';
        }
        return $this->response->setJSON($response);
    }
    public function save_work()
    {
        $model = new WorkModel();
        $jsonData = file_get_contents('php://input');
        $data_s = json_decode($jsonData, true);

        $data = [
            'name' => $data_s["name"], // Set the name value
            'color' => $data_s["color"], // Set the name value
            'purpose' => $data_s["purpose"], // Set the purpose value
            'program' => $data_s["program"], // Set the program value
            'department' => $data_s["department"], // Set the department value
            'start' => $data_s["start"], // Set the start value
            'end' => $data_s["end"], // Set the end value
            'hour' => '0', // Set the hour value
            'change_time' => '0' // Set the change_time value
        ];

        $insertedID = $model->insertWork($data);
        $response = [
            'id' => $insertedID,
        ];
        header('Content-Type: application/json');

        echo json_encode($response);
    }
    public function search()
    {
        $programModel = new ProgramModel();
        $data['results'] = $programModel;
        return view('search_results', $data);
    }
}
