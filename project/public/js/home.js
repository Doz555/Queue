var simple = {};
var work = {};
var Re_work = {
    old_id: 0
};

simple = sortTasks(simple);
var selectedCells = [];


function addTaskByCode() {
    if (Object.keys(simple).length === 0) {
        return;
    }
    $.ajax({
        url: '/programs/addQueue',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(simple),
        success: function(response) {
            if (response.status === 'success') {
                console.log(response.message);
            } else {
                console.error(response.message);
            }
        },
        error: function(xhr, status, error) {
            console.error(error);
        }
    });
    $('#job-table-body').html("");
    for (i in simple) {
        dateCode = simple[i].date;
        taskCode = simple[i].time;
        var tableBody = $('#job-table-body');
        const dateString = dateCode;
        let newRow;
        var existingRow = tableBody.find('tr td:first-child');
        if (existingRow) {
            existingRow.each(function() {
                if ($(this).text() === dateString) {
                    newRow = $(this).parent();
                }
            });
        }
        if (!newRow) {
            newRow = $('<tr>');
            var dateCell = $('<td>').text(dateString);
            newRow.append(dateCell);
            for (var i = 1; i < 10; i++) {
                var cell = $('<td>').addClass((i == 5) ? "lunch-break" : "").text((i == 5) ? "พัก" : "");
                newRow.append(cell);
            }
            tableBody.append(newRow);
        }
        var cell = newRow.find('td');
        let old_id;
        for (var i = 0; i < taskCode.length; i++) {
            let ii = (i < 4) ? i + 1 : i + 2;
            if (cell[ii]) {
                if (taskCode[i] === 0) {
                    $(cell[ii]).text('');
                    $(cell[ii]).removeClass();
                    old_id = 0;
                } else {
                    if (old_id != work[taskCode[i]].id) {
                        $(cell[ii]).text(work[taskCode[i]].name);
                    } else {
                        $(cell[ii]).text('');
                    }
                    $(cell[ii]).css('backgroundColor', work[taskCode[i]].color);
                    $(cell[ii]).attr('id_w', work[taskCode[i]].id);
                    old_id = work[taskCode[i]].id;
                }
            }
        }
    }
}

function sortTasks(data) {
    var sortedKeys = Object.keys(data).sort(function(a, b) {
        var dateA = new Date(data[a].date);
        var dateB = new Date(data[b].date);
        return dateA - dateB;
    });
    var sortedData = {};
    sortedKeys.forEach(function(key) {
        sortedData[key] = data[key];
    });
    return sortedData;
}

function formatDate(date) {
    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const year = date.getFullYear();
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');
    return day + '/' + month + '/' + year + ' ' + hours + ':' + minutes;
}

function saveNewTask(S, E, N, C, callback) {
    $.ajax({
        url: '/programs/save_work',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            start: formatDate(S),
            end: formatDate(E),
            name: N,
            color: C,
            purpose: $("#purpose").val(),
            program: $("#program").val(),
            department: $("#department").val(),
        }),
        success: function(response) {
            work[JSON.parse(response).id] = {
                id: JSON.parse(response).id,
                name: N,
                color: C,
                start: formatDate(S),
                end: formatDate(E),
                purpose: $("#purpose").val(),
                program: $("#program").val(),
                department: $("#department").val(),
            }

            return callback(JSON.parse(response).id);
        },
        error: function(error) {
            return false;
        }
    });


}

function generateData() {
    const inputs = $('#myModal input[required]');
    let isValid = true;
    inputs.each(function() {
        if (!$(this).val()) {
            isValid = false;
            return false; // exit the loop
        }
    });
    if (!isValid) {
        alert('กรุณากรอกข้อมูลให้ครบ');
        return;
    }

    const startDatePicker = $('#start-date-picker');
    const startTimePicker = $('#start-time-picker');
    const endDatePicker = $('#end-date-picker');
    const endTimePicker = $('#end-time-picker');
    const start = new Date(startDatePicker.val() + ' ' + startTimePicker.val() + ':00');
    const end = new Date(endDatePicker.val() + ' ' + endTimePicker.val() + ':00');
    const currentDate = new Date();
    currentDate.setHours(07)
    currentDate.setFullYear(start.getFullYear() + 543);
    start.setFullYear(start.getFullYear() + 543);
    end.setFullYear(end.getFullYear() + 543);
    if (start > end || start < currentDate) {
        alert('วันและเวลาไม่ถูกต้อง');
        return;
    }

    saveNewTask(start, end, $('#taskName').val(), $('#color-preview').css('backgroundColor'), function(new_work_id) {
        let timeS = [];
        let Hour = 0;
        for (let d = new Date(start); d < end; d.setDate(d.getDate() + 1)) {
            for (let s = 1; s <= 24; s++) {
                const time1 = new Date(d.getFullYear(), d.getMonth(), d.getDate(), s, 0, 0, 0);
                if (s > 8 && s <= 17 && s !== 13) {
                    if ((time1 < end && time1 > start) || time1.getTime() === end.getTime()) {
                        timeS.push(new_work_id);
                        Hour++;
                    } else {
                        timeS.push(0);
                    }
                }
            }
        }
        work[new_work_id].hour = Hour;
        let d = new Date(start);
        Re_work["new_id"] = new_work_id;
        ReTask(d.toISOString().split("T")[0], timeS);
        $.ajax({
            url: '/work/update_hour',
            type: 'POST',
            dataType: 'json',
            data: {
                id: new_work_id,
                hour: Hour,
            },
            success: function(response) {
                if (response.status === 'success') {
                    console.log(response.message);
                } else {
                    console.error(response.message);
                }
            },
            error: function(xhr, status, error) {
                console.error(error);
            }
        });
        jsonData = JSON.stringify(Re_work);
        console.log(jsonData);
        re_work();

        addTaskByCode();
        $('#But_Modal2').click();
    });
}

function re_work() {
    $.ajax({
        url: '/work/updateTime',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(Re_work),
        success: function(response) {
            if (response.status === 'success') {
                console.log(response.message);
            } else {
                console.error(response.message);
            }
        },
        error: function(xhr, status, error) {
            console.error(error);
        }
    });

    Re_work = {
        old_id: 0
    };
}

function ReTask(date, backToTheFuture) {
    if (simple[date] == undefined) {
        simple[date] = {
            date: date,
            time: [0, 0, 0, 0, 0, 0, 0, 0]
        }
        simple = sortTasks(simple);
    }
    for (let w_id in simple[date].time) {
        if (simple[date].time[w_id] != 0 && backToTheFuture.length > 0 && backToTheFuture[0] != 0) {
            let changed = false;
            for (let ii in backToTheFuture) {
                if (backToTheFuture[ii] == 0) {
                    backToTheFuture[ii] = simple[date].time[w_id];
                    changed = true;
                    break;
                }
            }
            if (!changed) {
                backToTheFuture.push(simple[date].time[w_id]);
            }
            simple[date].time[w_id] = backToTheFuture[0];
            backToTheFuture.shift()
        } else if (simple[date].time[w_id] != 0 && backToTheFuture.length > 0 && backToTheFuture[0] == 0) {
            backToTheFuture.shift()
        } else if (simple[date].time[w_id] == 0 && backToTheFuture.length > 0) {
            simple[date].time[w_id] = backToTheFuture[0];
            backToTheFuture.shift()
        }
        let work_id = simple[date].time[w_id];
        if (Re_work["old_id"] != work_id) {
            let w_ids = parseInt(w_id);
            var dates = new Date(date);
            (w_ids > 3) ? dates.setHours(w_ids + 10): dates.setHours(w_ids + 9);
            if (Re_work[work_id] == undefined) {
                dates.setHours(dates.getHours() - 1);
                Re_work[work_id] = {
                    start: formatDate(dates),
                    id: work_id,
                }
                if (Re_work["old_id"] != 0) {
                    if (w_ids == 0) {
                        dates.setDate(dates.getDate() - 1)
                        dates.setHours(17)
                        Re_work[Re_work["old_id"]].end = formatDate(dates)
                    } else {
                        ((w_ids - 1) > 3) ? dates.setHours(w_ids + 9): dates.setHours(w_ids + 8);
                        //dates.setHours(dates.getHours() + 1);
                        Re_work[Re_work["old_id"]].end = formatDate(dates);
                    }
                }

            } else {
                if (Re_work["old_id"] != 0) {
                    if (w_ids == 0) {
                        dates.setDate(dates.getDate() - 1)
                        dates.setHours(17)
                        Re_work[Re_work["old_id"]].end = formatDate(dates)
                    } else {
                        ((w_ids - 1) > 3) ? dates.setHours(w_ids + 9): dates.setHours(w_ids + 8);
                        Re_work[Re_work["old_id"]].end = formatDate(dates);
                    }
                }
            }
        }
        Re_work["old_id"] = work_id;
    }
    if (backToTheFuture.length > 0) {
        var dates = new Date(date);
        dates.setDate(dates.getDate() + 1);
        let NextDate = dates.toISOString().split("T")[0];
        ReTask(NextDate, backToTheFuture)
    }
}

$(document).ready(function() {
    $('#decreaseTimeBtn').click(function() {
        let parts = $('#work_end').text().split(' '); // Split the date and time parts
        let dateParts = parts[0].split('/'); // Split the date into day, month, and year
        let timeParts = parts[1].split(':'); // Split the time into hours and minutes
        let date = new Date(dateParts[2], dateParts[1] - 1, dateParts[0], timeParts[0], timeParts[1]);
        let time = [0, 0, 0, 0, 0, 0, 0, 0]
        if (timeParts[0] == '17') {
            date.setHours(9);
            date.setDate(date.getDate() + 1);
            time[0] = parseInt($('#work_id').text())
        } else if (timeParts[0] == '12' || timeParts[0] == '13') {
            date.setHours(14);
            time[4] = parseInt($('#work_id').text())
        } else {
            date.setHours(date.getHours() + 1);
            if (date.getHours() > 12) {
                time[date.getHours() - 10] = parseInt($('#work_id').text())
            } else {
                time[date.getHours() - 9] = parseInt($('#work_id').text())
            }

        }
        $('#work_end').text(formatDate(date))
        $('#work_hour').text((parseInt($('#work_hour').text()) + 1) + " ชั่วโมง")
        ReTask(date.toISOString().split("T")[0], time);
        re_work();
        addTaskByCode();
        $.ajax({
            url: '/work/EditTime',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify({
                id: $('#work_id').text(),
                end: $('#work_end').text(),
                hour: $('#work_hour').text(),
                change_time: 1,
            }),
            success: function(response) {
                if (response.status === 'success') {
                    console.log(response.message);
                } else {
                    console.error(response.message);
                }
            },
            error: function(xhr, status, error) {
                console.error(error);
            }
        });
    });

    $('#increaseTimeBtn').click(function() {
        let parts = $('#work_end').text().split(' '); // Split the date and time parts
        let dateParts = parts[0].split('/'); // Split the date into day, month, and year
        let timeParts = parts[1].split(':'); // Split the time into hours and minutes
        let date = new Date(dateParts[2], dateParts[1] - 1, dateParts[0], timeParts[0], timeParts[1]);

        if (parseInt($('#work_hour').text()) - 1 <= 0) {
            var result = confirm("หากลดลงอีกจะเท่ากับลบ");
            if (result === true) {
                if (date.getHours() > 12) {
                    simple[date.toISOString().split("T")[0]].time[date.getHours() - 10] = 0;
                } else {
                    simple[date.toISOString().split("T")[0]].time[date.getHours() - 9] = 0;
                }

                $('#work_end').text(formatDate(date))
                $('#work_hour').text((parseInt($('#work_hour').text()) - 1) + " ชั่วโมง")
                $('#But_Modal1').click();
                addTaskByCode();
            }
        } else {
            if (timeParts[0] == '09') {
                simple[date.toISOString().split("T")[0]].time[0] = 0;
                date.setHours(17);
                date.setDate(date.getDate() - 1);

            } else if (timeParts[0] == '14' || timeParts[0] == '13') {
                simple[date.toISOString().split("T")[0]].time[4] = 0;
                date.setHours(12);
            } else {
                if (date.getHours() > 12) {
                    simple[date.toISOString().split("T")[0]].time[date.getHours() - 10] = 0;
                } else {
                    simple[date.toISOString().split("T")[0]].time[date.getHours() - 9] = 0;
                }
                date.setHours(date.getHours() - 1);
            }

            $('#work_hour').text((parseInt($('#work_hour').text()) - 1) + " ชั่วโมง")
            addTaskByCode();
        }

        for (let d in simple) {
            console.log(d);
            ReTask(d, [])
        }
        try {
            $('#work_end').text(Re_work[$('#work_id').text()].end);
        } catch {
            $('#work_end').text("");
        }

        re_work();
        $.ajax({
            url: '/work/EditTime',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify({
                id: $('#work_id').text(),
                hour: $('#work_hour').text(),
                change_time: -1
            }),
            success: function(response) {
                if (response.status === 'success') {
                    console.log(response.message);
                } else {
                    console.error(response.message);
                }
            },
            error: function(xhr, status, error) {
                console.error(error);
            }
        });

    });


    $('#hue-slider').on('input', function() {
        $('#color-preview').css('backgroundColor', 'hsl(' + $(this).val() + ', 100%, 83%)');
    });

    const currentDate = new Date();
    const year = currentDate.getFullYear().toString();
    const month = (currentDate.getMonth() + 1).toString().padStart(2, '0');
    const day = currentDate.getDate().toString().padStart(2, '0');
    const currentDateString = year + '-' + month + '-' + day;

    $('#start-date-picker').val(currentDateString);
    $('#end-date-picker').val(currentDateString);

    $('#job-table').on('click', 'td:not(:first-child):not(:nth-child(6))', function() {
        $.ajax({
            url: '/programs/get_data',
            type: 'GET',
            data: {
                id: $(this).attr('id_w'),
            },
            success: function(response) {
                $('#myModal1').find('div.modal-body').html(response);
                $('#But_Modal1').click();
            },
            error: function() {
                console.log('Error occurred during AJAX request');
            }
        });
    });
    addTaskByCode();
});